## Installation

Install nvm. See [here](https://github.com/nvm-sh/nvm)

`nvm install 12.6.1`

 `yarn`

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

To run all tests<br />