import axios from 'axios'
import { expireAuthentication } from './auth'

// Actions
export const REQUEST = 'Tasks/REQUEST'
export const RECEIVED = 'Tasks/RECEIVED'
export const RECEIVED_TASK = 'Tasks/RECEIVED_TASK'
export const FAILED = 'Tasks/FAILED'
export const DELETED = 'Tasks/DELETED'

// Action Creators
export function fetchTasks() {
  return (dispatch, getState) => {
    const { auth } = getState()
    dispatch(requestTasks())
    return axios({
      url: '/api/v1/tasks',
      headers: {
        'access-token': auth.accessToken,
        'client': auth.client,
        'uid': auth.uid,
        'expiry': auth.expiry,
        'token-type': 'Bearer'
      }
    }).then(response => {
      dispatch(receiveTasks(response.data.tasks))
    }).catch(error => {
      dispatch(failFetchTasks())
      if (error.response && error.response.status === 401) {
        dispatch(expireAuthentication())
      }
    })
  }
}

export function createTasks(description, avatar_url) {
  return (dispatch, getState) => {
    const { auth } = getState()
    dispatch(requestTasks())
    return axios({
      url: '/api/v1/tasks',
      method: 'POST',
      headers: {
        'access-token': auth.accessToken,
        'client': auth.client,
        'uid': auth.uid,
        'expiry': auth.expiry,
        'token-type': 'Bearer'
      },
      data: { description, avatar_url }
    }).then(response => {
      dispatch(receiveTask(response.data.task))
    }).catch(error => {
      dispatch(failFetchTasks())
      if (error.response && error.response.status === 401) {
        dispatch(expireAuthentication())
      }
    })
  }
}

export function updateTask(data) {
  return (dispatch, getState) => {
    const { auth } = getState()
    return axios({
      url: `/api/v1/tasks/${data.id}`,
      method: 'PATCH',
      headers: {
        'access-token': auth.accessToken,
        'client': auth.client,
        'uid': auth.uid,
        'expiry': auth.expiry,
        'token-type': 'Bearer'
      },
      data: { done: data.done }
    }).then(response => {
      dispatch(receiveTask({...data, ...response.data}))
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        dispatch(expireAuthentication())
      }
    })
  }
}

export function deleteTask(id) {
  return (dispatch, getState) => {
    const { auth } = getState()
    return axios({
      url: `/api/v1/tasks/${id}`,
      method: 'DELETE',
      headers: {
        'access-token': auth.accessToken,
        'client': auth.client,
        'uid': auth.uid,
        'expiry': auth.expiry,
        'token-type': 'Bearer'
      }
    }).then(response => {
      dispatch(deletedTask(id))
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        dispatch(expireAuthentication())
      }
    })
  }
}

function requestTasks() {
  return { type: REQUEST }
}

function receiveTasks(tasks) {
  return { type: RECEIVED, tasks }
}
function receiveTask(task) {
  return { type: RECEIVED_TASK, task }
}

function failFetchTasks() {
  return { type: FAILED }
}

function deletedTask(id) {
  return { type: DELETED, id }
}
