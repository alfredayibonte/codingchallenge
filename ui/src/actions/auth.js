import axios from 'axios'

// Actions
export const REQUEST = 'AUTH/REQUEST'
export const RECEIVED = 'AUTH/RECEIVED'
export const FAILED = 'AUTH/FAILED'
export const SIGNOUT = 'AUTH/SIGNOUT'

// Action Creators
export function authenticate(email, password) {
  return (dispatch, getState) => {
    dispatch(startAuthentication())
    return axios({
      url: '/api/v1/auth/sign_in',
      method: 'POST',
      data: { email, password }
    }).then(response => {
      const uid = response.headers['uid']
      const client = response.headers['client']
      const accessToken = response.headers['access-token']
      const expiry = response.headers['expiry']
      dispatch(successAuthentication(uid, client, accessToken, expiry))
    }).catch(error => {
      dispatch(failAuthentication())
    })
  }
}

export function registerUser({email, password, passwordConfirmation, name}) {
  return (dispatch, getState) => {
    dispatch(startAuthentication())
    return axios({
      url: '/api/v1/auth',
      method: 'POST',
      data: {email, password}
    }).then(response => {
      const uid = response.headers['uid']
      const client = response.headers['client']
      const accessToken = response.headers['access-token']
      const expiry = response.headers['expiry']
      dispatch(successAuthentication(uid, client, accessToken, expiry))
    }).catch(error => {
      dispatch(failAuthentication())
    })
  }
}

export function signout() {
  return (dispatch, getState) => {
    const { auth } = getState()
    return axios({
      url: '/api/v1/auth/sign_out',
      method: 'DELETE',
      headers: {
        'access-token': auth.accessToken,
        'client': auth.client,
        'uid': auth.uid
      }
    }).then(response => {
      dispatch(doSignout())
    }).catch(error => {
      console.log(error)
    })
  }
}

export function expireAuthentication() {
  return doSignout()
}

function startAuthentication() {
  return { type: REQUEST }
}

function successAuthentication(uid, client, accessToken, expiry) {
  return { type: RECEIVED, uid, client, accessToken, expiry }
}

function failAuthentication() {
  return { type: FAILED }
}

function doSignout() {
  return { type: SIGNOUT }
}
