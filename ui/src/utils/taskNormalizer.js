/**
 * 
 * @param [] data eg. [{id: 1, description: ""}, ...]
 * @returns {} data eg. {1: {description: ""}, 2: {}, ...}
 */
export const taskNormalizer = (data=[]) => {
  return data.reduce((acc, value) => {
    acc[value.id] = value;
    return acc;
  }, {})
}
