export const isValid = (state) => {
  return Object.keys(state).reduce((acc, key) => {
    acc = acc && state && state[key] && (state[key].trim().length > 1 );
    return acc;
  }, true );
}
