// const proxy = require('http-proxy-middleware');
const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://localhost:4000',
      changeOrigin: true,
    })
  );
  // app.use(
  //   createProxyMiddleware('/api/v1/tasks/', {
  //     target: 'http://localhost:4000/',
  //     changeOrigin: true
  //   })
  // );
};
