import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import Home from '../components/TaskList'
import Signup from '../pages/Signup';
import Login from '../pages/Login/';
import './App.css';
import AddTask from '../pages/AddTask';
import PrivateRoute from '../components/privateRoute';

function App() {
  return (
    <BrowserRouter>
      <div className="App" id="layout" data-testid="layout">
        <div>
          <PrivateRoute exact path="/" component={Home} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={Login} />
          <Switch>
            <PrivateRoute exact path="/tasks" component={Home} />
            <PrivateRoute path="/tasks/new" component={AddTask} />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}
export default App;
