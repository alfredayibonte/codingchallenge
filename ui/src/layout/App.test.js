import React from 'react';
import { mount } from 'enzyme';
import App from './App';
import { wrapper } from '../testUtils'

test('renders learn react link', () => {
  const component = mount(wrapper(<App />));
  const layout = component.find('#layout');
  expect(layout).toHaveLength(1)
});
