import moxios from 'moxios';
import { testStore } from '../testUtils';
import { fetchTasks } from '../actions/task';

describe('fetchTask action', () => {

    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    test('Store is updated correctly', () => {

        const expectedState = [
          {
            id: 1,
            description: 'Example title 1',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          },
          {
            id: 2,
            description: 'Example title 2',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          },
          {
            id: 3,
            description: 'Example title 3',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          }
        ];

        const output = {
          1: {
            id: 1,
            description: 'Example title 1',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          },
          2: {
            id: 2,
            description: 'Example title 2',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          },
          3: {
            id: 3,
            description: 'Example title 3',
            "avatar_url": "url",
            "done": null,
            "user_id": 1,
            "created_at": "2020-08-09T13:26:21.236Z",
            "updated_at": "2020-08-09T17:41:29.150Z"
          }
        }
        const store = testStore();

        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: {tasks: expectedState}
            })
        });

        return store.dispatch(fetchTasks())
        .then(() => {
            const newState = store.getState();
            expect(newState.task).toEqual({loading: false, tasks: output});
        })
        
    });

});
