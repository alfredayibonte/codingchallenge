import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from "history";
import auth from './auth'
import task from './task'

export const history = createBrowserHistory()
const rootReducer = combineReducers({
  auth,
  task,
  router: connectRouter(history)
})

export default rootReducer;
