import { taskNormalizer } from '../utils/taskNormalizer';
import { REQUEST, RECEIVED, FAILED, DELETED, RECEIVED_TASK } from '../actions/task';

const initialState = {
  loading: false,
  tasks: {}
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST:
      return  {...state, loading: false }
    case RECEIVED:
      return {
        loading : false,
        tasks: { ...state.tasks, ...taskNormalizer(action.tasks)}
      }
    case RECEIVED_TASK:
      return {
        loading : false,
        tasks: { ...state.tasks, [action.task.id]: action.task}
      }
    case FAILED:
      return {...state, loading: false }
    case DELETED:
      const tasks = {...state.tasks };
      delete tasks[action.id];
      return { loading: false, tasks };

    default: return state;
  }
}
