import { RECEIVED } from '../actions/task';
import tasksReducer from './task';

const defaultState = {
  loading: false,
  tasks: {}
}

describe('Tasks Reducer', () => {

    it('Should return default state', () => {
        const newState = tasksReducer(undefined);
        expect(newState).toEqual(defaultState);
    });

    it('Should return new state if receiving type', () => {

      const tasks = [
        { id: 1, description: 'Test 1'},
        { id: 2, description: 'Test 2'},
        { id: 3, description: 'Test 3'}
      ];
      const newState = tasksReducer(defaultState, {
          type: RECEIVED,
          tasks
      });
      const output = {
        loading: false,
        tasks: {
          1: {id: 1, description: 'Test 1'},
          2: {id: 2, description: 'Test 2'},
          3: {id: 3, description: 'Test 3'},
        },
      }
      expect(newState).toEqual(output);
    });

});
