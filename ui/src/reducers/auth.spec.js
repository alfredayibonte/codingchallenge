import authReducer from './auth';

const initialState = {
  loading: false,
  uid: undefined,
  isAuthenticated: false,
  accessToken: undefined,
  client: undefined,
  expiry: undefined,
  failed: false
}

describe('Auth Reducer', () => {
  it('Should return default state', () => {
      const newState = authReducer(undefined);
      expect(newState).toEqual(initialState);
  });

});
