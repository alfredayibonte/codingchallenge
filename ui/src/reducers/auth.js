import { REQUEST, RECEIVED, FAILED, SIGNOUT } from '../actions/auth';

const initialState = {
  loading: false,
  uid: undefined,
  isAuthenticated: false,
  accessToken: undefined,
  client: undefined,
  expiry: undefined,
  failed: false
}

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST:
      return { ...state, loading: true, failed: false }
    case RECEIVED:
      return  {
        ...state, loading: false,
        isAuthenticated: true,
        uid: action.uid,
        client: action.client,
        accessToken: action.accessToken,
        expiry: action.expiry,
        failed: false,
      }
    case FAILED:
      return { ...state, loading: false, failed: true }
    case SIGNOUT:
      return { ...initialState}
    default: return state
  }
}
