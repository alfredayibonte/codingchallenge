import React, {useState} from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { isValid } from '../../utils/validation';
import { registerUser } from '../../actions/auth'
import ErrorView from '../../components/Error'
import './style.scss';

const Signup = ({isAuthenticated, registerUser, loading}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    password: '',
    passwordConfirmation: '',
  })
  const [error, setError] = useState(false);
  const handleInputChange = (name) => (event) => {
    const {value} = event.target;
    setState({...state, [name]: value})
  }
  const handleSubmit = ()  => {
    const { name, email, password, passwordConfirmation } = state
    const valid = isValid(state);
    if(!valid){
      setError(true)
      return;
    }
    registerUser(name, email, password, passwordConfirmation);
  }

  if (isAuthenticated) {
    return <Redirect to="/" />
  }

  return (
    <div className="signupContainer">
      <h2>Signup</h2>
      {error && <ErrorView>Enter all fields correctly</ErrorView>}
      {loading && <CircularProgress className="spinner"/>}
        <div className="input">
          <TextField
            onChange={handleInputChange("name")}
            id="standard-name-indivut"
            value={state.name}
            label="Name"
            type="text"
            autoComplete="current-name"
          />
        </div>
        <div className="input">
          <TextField
            className="input"
            onChange={handleInputChange("email")}
            id="standard-email-input"
            value={state.email}
            label="Email"
            type="text"
            autoComplete="current-email"
          />
        </div>
        <div className="input">
          <TextField
            className="input"
            onChange={handleInputChange("password")}
            id="standard-password-input"
            value={state.password}
            label="Password"
            type="password"
            autoComplete="current-password"
          />
        </div>
        <div className="input">
          <TextField
            className="input"
            onChange={handleInputChange("passwordConfirmation")}
            id="standard-password-confirmation-input"
            value={state.passwordConfirmation}
            label="Confirm Password"
            type="password"
            autoComplete="current-confirmation-password"
          />
        </div>
      <p className="login">Already have an account? <Link to="/login"> Login </Link></p>
      <div>
        <Button variant="contained" onClick={handleSubmit}>Sign up</Button>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { auth } = state
  const { isAuthenticated, loading } = auth
  return { isAuthenticated, loading }
}

const mapDispatchToProps = {
  registerUser,
}
export default connect(mapStateToProps, mapDispatchToProps)(Signup)
