import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import ErrorView from '../../components/Error';
import { isValid } from '../../utils/validation';
import { authenticate } from '../../actions/auth'
import './style.scss'

const Login = ({isAuthenticated, authenticate, loading, failed}) => {
  const [error, setError] = useState(false)
  const [credentials, setCredentials] = useState({
    email: '',
    password: ''
  })

  const handleChange = (name) => (event) => {
    const {value} = event.target;
    setCredentials({
      ...credentials,
      [name]: value,
    })
  }

  const handleSubmit = () => {
    const valid = isValid(credentials);
    if(!valid){
      setError(true)
      return;
    }
    const { email, password} = credentials;
    authenticate(email, password)
    
  }

  if (isAuthenticated) {
    return <Redirect to="/" />
  }
  return (
    <div className="loginContainer">
        <h2>Login</h2>
        {error && <ErrorView>Enter all fields correctly</ErrorView>}
        {failed && <ErrorView>Invalid login credentials. Please try again</ErrorView>}
        {loading && <CircularProgress className="spinner"/>}
        <div className="input">
          <TextField
            onChange={handleChange("email")}
            id="standard-email-input"
            value={credentials.email}
            label="Email"
            type="email"
            autoComplete="current-email"
          />
        </div>
        <div className="input">
          <TextField
            onChange={handleChange("password")}
            id="standard-password-input"
            value={credentials.password}
            label="Password"
            type="password"
            autoComplete="current-password"
          />
        </div>
        <p className="signup">Click <Link to="/signup"> here </Link> to signup</p>
        <div>
          <Button variant="contained" onClick={handleSubmit}>Login</Button>
        </div>
      </div>
  )
}

const mapStateToProps = (state) => {
  const { auth } = state
  const { loading, isAuthenticated, failed } = auth
  return {
    loading,
    isAuthenticated,
    failed
  }
}

const mapDispatchToProps = {
  authenticate,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
