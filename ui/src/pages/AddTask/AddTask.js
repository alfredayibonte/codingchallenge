import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import Header from '../../components/Header';
import {createTasks} from '../../actions/task';
import {isValid} from '../../utils/validation'
import './style.scss'

const AddTask = ({createTasks, history}) => {
  const [state, setState] = useState({description: '', avatar: ''})
  const submit = () => {
    if(isValid(state)) {
      const { description, avatar} = state;
      createTasks(description, avatar).then(
        (res) => history.push('/')
      )
    }
  }
  const handleChange = (name) => (event) => {
    const { value } = event.target;
    setState({ ...state, [name]: value});
  }

  return (
    <div>
      <Header title="Add Task" />
      <div className="task">
        <TextField
          onChange={handleChange("description")}
          id="standard-full-width"
          label="Task Description"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          onChange={handleChange("avatar")}
          id="standard-full-width"
          label="Avatar URL"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Button onClick={submit} className="submit" variant="contained" color="primary">
          Add
        </Button>
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  const { task } = state
  const { loading, tasks } = task
  return {
    loading,
    tasks
  }
}

const mapDispatchToProps = {
  createTasks,
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);
