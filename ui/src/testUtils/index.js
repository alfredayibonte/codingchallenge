import checkPropTypes from 'check-prop-types';
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import React from 'react';
import { applyMiddleware, createStore, compose } from 'redux';
import persistState from 'redux-localstorage'
import rootReducer, {history} from '../reducers';
import { middlewares } from '../store';

export const checkProps = (component, expectedProps) => {
  const propsErr = checkPropTypes(component.propTypes, expectedProps, 'props', component.name);
  return propsErr;
};

export const findByTestAtrr = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
};

const enhancer = compose(
  applyMiddleware(...middlewares),
  persistState('auth', { key: 'AUTH' })
)

export const testStore = (initialState={}) => {
  return createStore(rootReducer, initialState, enhancer);
};

export const wrapper = (component) => {
  return (
    <Provider store={testStore()}>
      <ConnectedRouter history={history}>
        {component}
      </ConnectedRouter>
    </Provider>
  )
}
