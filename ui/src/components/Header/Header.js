import React from 'react';

import './styles.scss'

const Header = ({title, children}) => {
  return (
    <header data-testid="header" className="header">
    <span id="title">{title}</span>
      <div className="body">
        {children}
      </div>
    </header>)
}
export default Header;
