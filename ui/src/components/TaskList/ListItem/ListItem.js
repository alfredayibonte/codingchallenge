import React, {useState} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import './style.scss'

const ListItemView = ({data, updateTask}) => {
  const [checked, setChecked] = useState(false)
  const handleCheckClicked = (event) => {
    setChecked(event.target.checked)
    const { id } = data;
    updateTask({ id, done: true })
  }
  
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar alt={data.name && data.name.charAt(0)} src={data.avatar_url} />
      </ListItemAvatar>
      <ListItemText
        primary={data.name? data.name: 'username'}
        secondary={
          <React.Fragment>
            <Typography
              component="span"
              variant="body2"
              className=""
              color="textPrimary"
            >
            {data.title}
            </Typography>
            {data.description}
          </React.Fragment>
          }
        />
      <ListItemSecondaryAction>
        <ListItemIcon>
        {
          data.done ?
            <span className="date">
              {moment(data.updated_at).format('h:mm a')}
            </span>
          :
            <Checkbox
              checked={checked}
              onClick={handleCheckClicked}
              tabIndex={-1}
              disableRipple
              inputProps={{ 'aria-labelledby': "labelId" }}
            />
        }
          
      </ListItemIcon>
      </ListItemSecondaryAction>
    </ListItem>
  )
  }
  ListItem.propTypes = {
    data: PropTypes.object.isRequired,
    updateTask: PropTypes.func.isRequired
  }
export default ListItemView;
