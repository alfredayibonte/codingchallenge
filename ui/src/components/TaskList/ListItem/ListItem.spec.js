import React from 'react';
import {  checkProps } from '../../../testUtils';
import ListItem from './index';

describe('ListItem Component', () => {

    describe('Checking PropTypes', () => {

      it('Should NOT throw a warning', () => {
          const expectedProps = {
              data: {id: 1, description: "some description", avatar_url: 'some_url.jpeg'},
              updateTask: () => {},
              emitEvent: () => {
              }
          };
          const propsError = checkProps(ListItem, expectedProps);
          expect(propsError).toBeUndefined();
      });

    });
});
