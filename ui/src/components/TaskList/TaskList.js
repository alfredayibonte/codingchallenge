import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import List from '@material-ui/core/List';
import { fetchTasks, updateTask } from '../../actions/task';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import ListItem from './ListItem';
import './style.scss';

const TaskList = ({fetchTasks, updateTask, tasks, loading, history}) => {
  useEffect(() => {
    fetchTasks()
  }, [tasks.length])
  const gotoAddTask = () => {
    history.push('/tasks/new')
  }
  return (
    <div id="taskList">
      <Header title="Tasks">
        <IconButton className="white" aria-label="add new task" onClick={gotoAddTask}>
          <AddIcon />
        </IconButton>
      </Header>
      {loading &&  <Loader />}
      <List className="tasks" data-testid="task-list">
      {
        tasks && (Object.keys(tasks).length > 0) &&
          Object.keys(tasks).map(
          (id) => <ListItem  key={id} data={tasks[id]} updateTask={updateTask}/>)
      }
      </List>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { task } = state;
  const { loading, tasks } = task;
  return {
    loading,
    tasks
  }
}

const mapDispatchToProps = {
  fetchTasks,
  updateTask
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
