import React from 'react';
import { mount } from 'enzyme';
import { wrapper } from '../../testUtils'
import TaskList from './TaskList';

describe('TaskList Component', () => {

    describe('Renders', () => {
      it('', () => {
        const component = mount(wrapper(<TaskList />));
        const layout = component.find('#taskList');
        expect(layout).toHaveLength(1);
      })
    });
});
