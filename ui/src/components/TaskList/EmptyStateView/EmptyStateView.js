import React from 'react';
import './style.scss';

const EmptyStateView = () => {
  return (<div className="empty-state">No item add yet</div>)
}
export default EmptyStateView;
