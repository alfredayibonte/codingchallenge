import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import './style.scss';

const Loader = () => <CircularProgress className="spinner"/>;
export default Loader;
