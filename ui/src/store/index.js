import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router'
import persistState from 'redux-localstorage'
import ReduxThunk from 'redux-thunk';
import rootReducer, {history} from '../reducers';

export const middlewares = [ReduxThunk, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');
  middlewares.push(logger)
}

const enhancer = compose(
  applyMiddleware(...middlewares),
  persistState('auth', { key: 'AUTH' })
)

const store = createStore(rootReducer, {}, enhancer);
export default store;
