# frozen_string_literal: true

module Api
  module V1
    class TasksController < Api::BaseController
      before_action :set_task, only: [:show, :update, :destroy]
      before_action :authenticate_api_v1_user!, only: [:create, :update, :destroy]

      def index
        if current_api_v1_user
          render json: { tasks: Task.find_user_task(current_api_v1_user.id) }
        else
          render json: { tasks: Task.all }
        end
      end

      def create
        task = Task.new(task_params)
        task.user_id = current_api_v1_user.id

        if task.save!
          render json: task, status: :created
        else
          render json: task.errors, status: :unprocessable_entity
        end
      end

      def show
        render json: @task
      end

      def update
        if @task.update(task_params)
          render json: @task
        else
          render json: @task.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @task.destroy
      end

      private
        def task_params
          params.permit(:description, :avatar_url, :done)
        end

        def set_task
          @task = Task.find(params[:id])
        end
    end
  end
end
