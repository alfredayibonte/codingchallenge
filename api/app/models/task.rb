# frozen_string_literal: true

class Task < ApplicationRecord
  validates :description, presence: true
  validates :avatar_url, presence: true
  belongs_to :user

  def self.find_user_task(user_id)
    select("users.id, users.email, users.name, tasks.description,tasks.created_at, tasks.updated_at, tasks.avatar_url, tasks.done")
    .joins("join users on users.id = tasks.user_id")
    .where(user_id: user_id)
  end
end
