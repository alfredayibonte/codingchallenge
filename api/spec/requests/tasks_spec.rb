# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/tasks", type: :request do
  let(:user) {
    FactoryBot.create(:user, email: "user@example.com", password: "12345678")
  }
  let(:invalid_attributes) {
    FactoryBot.attributes_for(:task, description: nil)
  }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:task, user: user)
  }

  let(:valid_headers) {
    {
      "uid": response.headers["uid"],
      "client": response.headers["client"],
      "access-token": response.headers["access-token"],
      "expiry": response.headers["expiry"]
    }
  }

  before(:each) do
    @login_url = "/api/v1/auth/sign_in"
    @login_params = {
      email: user.email,
      password: "12345678"
    }
    post @login_url, params: @login_params
  end

  describe "GET /index" do
    it "renders a successful response" do
      Task.create! valid_attributes
      get api_v1_tasks_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      task = Task.create! valid_attributes
      get api_v1_task_url(task), as: :json
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Task" do
        expect {
          post api_v1_tasks_url,
               params: valid_attributes, headers: valid_headers, as: :json
        }.to change(Task, :count).by(1)
      end

      it "renders a JSON response with the new task" do
        post api_v1_tasks_url,
             params: valid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Task" do
        expect {
          post api_v1_tasks_url,
          params: invalid_attributes, as: :json
        }.to change(Task, :count).by(0)
        expect { raise ActiveRecord::RecordInvalid }.to raise_error
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested task" do
        task = Task.create! valid_attributes
        patch api_v1_task_url(task),
              params: valid_attributes, headers: valid_headers, as: :json
        task.reload
        expect(response.status).to be(200)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
    end

    context "with invalid parameters" do
      it "renders a JSON response with errors for the task" do
        task = Task.create! valid_attributes
        patch api_v1_task_url(task),
              params: invalid_attributes, headers: valid_headers, as: :json
        expect(response.status).to be(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested task" do
      task = Task.create! valid_attributes
      expect {
        delete api_v1_task_url(task), headers: valid_headers, as: :json
      }.to change(Task, :count).by(-1)
    end
  end
end
