# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    avatar_url { "url" }
    description { "some descrition" }
    trait :first_user_task do
      user { 1 }
    end
  end
end
