# Getting started

## Installation
- Install rvm. follow link [here](https://rvm.io)

- `rvm install 2.7.1`

- Install bundler
`gem install bundler`

- Install rails
`gem install rails`

- `rails db:create db:migrate db:seed`

## start serve
`rails s -p 4000`